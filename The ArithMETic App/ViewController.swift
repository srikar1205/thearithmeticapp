//
//  ViewController.swift
//  The ArithMETic App
//
//  Created by Student on 2/14/19.
//  Copyright © 2019 Student. All rights reserved.
//

import UIKit

class ViewController: UIViewController, UIPickerViewDelegate, UIPickerViewDataSource{
    
    
    @IBOutlet weak var picker: UIPickerView!
    @IBOutlet weak var weightTF: UITextField!
    @IBOutlet weak var exerciseTF: UITextField!
    @IBOutlet weak var caloriesLBL: UILabel!
    @IBOutlet weak var timePoundLBL: UILabel!
    
    
    @IBAction func clear(_ sender: Any) {
        weightTF.text! = ""
        exerciseTF.text! = ""
        caloriesLBL.text! = "0 cal"
        timePoundLBL.text! = "0 minutes"
        picker.selectRow(0, inComponent: 0, animated: true)
    }
    
    
    
    var pickerValue: [String] = [String]()
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        self.view.backgroundColor = #colorLiteral(red: 0.8659811616, green: 0.681150496, blue: 0.8648683429, alpha: 1)
        
        self.picker.delegate = self
        self.picker.dataSource = self
        pickerValue = ["Bicycling", "Jumping rope", "Running - slow", "Running - fast", "Tennis", "Swimming"]
    }
    
    func energyConsumed(during: String, weight:Double, time:Double) -> Double {
        var activity = 0.0
        switch during{
        case "Bicycling","Tennis" : activity = 8.0
        case "Jumping rope" : activity = 12.3
        case "Running - slow" : activity = 9.8
        case "Running - fast" : activity = 23.0
        case "Swimming" : activity = 5.8
        default : activity = 0.0
            
        }
        let weightinkgs = Double(weight)/2.2
        return activity * 3.5 * weightinkgs/Double(200) * time
        
    }
    func timeToLose1Pound(during: String, weight: Double) -> Double {
        let energyConsumedInCalories = energyConsumed(during: during, weight: weight, time: 1.0)
        if weight == 0.0 {
            return 0
        }
        else{
        return Double(3500)/energyConsumedInCalories
        }
    }
    
    @IBAction func calculate(_ sender: Any) {
        let generatedvalue = pickerValue[picker.selectedRow(inComponent: 0)]
        let finalvalue = 0.0
        let energy = energyConsumed(during: generatedvalue, weight: Double(weightTF.text!) ?? finalvalue, time: Double(exerciseTF.text!) ?? finalvalue)
        caloriesLBL.text = String(format: "%.1f cal", energy)
        let timeToLooseOnePound = timeToLose1Pound(during: generatedvalue, weight: Double(weightTF.text!) ?? finalvalue)
        timePoundLBL.text = String(format: "%.1f minutes", timeToLooseOnePound)
    }
    
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return pickerValue.count
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        return pickerValue[row]
    }
}

